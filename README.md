# Book Reading Web
This is a web application built using JSP and Servlets. The application allows users to browse and read books from a library, as well as login as an admin to add, edit or delete books.


## Features
- Add books to the library: Admin users can add new books to the library. This feature is only available when logged in with an admin account.
- Edit book information: Admin users can edit the information of books already in the library.
- Delete books: Admin users have the ability to remove a book from the library.
- Search for books: Users can search for books by title, author, or related keywords.
- Read books: Users can open a book to read it directly on the application.
- Save read books: Users can save books they have previously read to review later.

## Requirements
- Java SE Development Kit (JDK)
- Apache Tomcat
- SQL Server
